<?php

namespace CruzDigital\PayGradeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PayGradeBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle'; 
	}
}
