<?php
// src/CruzDigital/PayGradeBundle/Entity/User.php

namespace CruzDigital\PayGradeBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="CruzDigital\PayGradeBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $firstName;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your first name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $lastName;

	 /**
     * @ORM\Column(type="integer", options={"default" = 0})
     *
     */
    protected $notoriety;
	 
	 /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="user")
     * @var Collection
     */
    private $ratings;

    public function __construct()
    {
		//Initializing collection. Doctrine recognizes Collections, not arrays!
     $this->ratings = new ArrayCollection();
        parent::__construct();
           
    }
	 //Getters and setters
     
    /** @return Collection */
    public function getRatings() {
        return $this->ratings;
    }
     
    /** @param Comment $comment */
    public function addRating(Rating $rating) {
        $this->ratings->add($rating);
        $rating->setUser($this);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set notoriety
     *
     * @param integer $notoriety
     * @return User
     */
    public function setNotoriety($notoriety)
    {
        $this->notoriety = $notoriety;

        return $this;
    }

    /**
     * Get notoriety
     *
     * @return integer 
     */
    public function getNotoriety()
    {
        return $this->notoriety;
    }

    /**
     * Remove ratings
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Rating $ratings
     */
    public function removeRating(\CruzDigital\PayGradeBundle\Entity\Rating $ratings)
    {
        $this->ratings->removeElement($ratings);
    }
}