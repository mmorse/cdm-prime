<?php
// src/CruzDigital/PayGradeBundle/Entity/Agency.php

namespace CruzDigital\PayGradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="agency")
 */
 
class Agency
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the Agency name.")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long."
     * )
     */
    protected $agencyName;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the Agency Address.")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The address is too short.",
     *     maxMessage="The address is too long."
     * )
     */
    protected $address1;
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $address2;
	 /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the Agency City.")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The city is too short.",
     *     maxMessage="The city is too long."
     * )
     */
    protected $city;
		/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the State.")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long."
     * )
     */
    protected $state;
	/**
     * @ORM\Column(type="string", length=10)
     *
     * @Assert\NotBlank(message="Please enter the State.")
     * @Assert\Length(
     *     min=5,
     *     max="255",
     *     minMessage="The zip is too short.",
     *     maxMessage="The zip is too long."
     * )
     */
    protected $zip;
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the Email.")
     * @Assert\Length(
     *     min=5,
     *     max="255",
     *     minMessage="The email is too short.",
     *     maxMessage="The email is too long."
     * )
     */
    protected $email;
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="The Billing Contact Name is too short.",
     *     maxMessage="The Billing Contact name is too long."
     * )
     */
    protected $billingContact;
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Length(
     *     min=6,
     *     max="50",
     *     minMessage="The Billing Phone Name is too short.",
     *     maxMessage="The Billing Phone name is too long."
     * )
     */
    protected $billingPhone;
	/**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\Length(
     *     min=6,
     *     max="50",
     *     minMessage="The Fax Phone Name is too short.",
     *     maxMessage="The Fax Phone name is too long."
     * )
     */
    protected $fax;
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $averageRating;
	 /**
     * @ORM\Column(type="integer")
     */
    protected $averageRatingCount;
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $averageOverallRating;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $overallRatingCount;
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $averagePaymentRating;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $paymentRatingCount;
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $averageDocumentRating;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $documentRatingCount;
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $averagePayRateRating;
		 /**
     * @ORM\Column(type="integer")
     */
    protected $payrateRatingCount;
	 /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="agency")
     * @var Collection
     */
    private $ratings;
	
	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $website;

	   public function setId($id)
    {
        $this->id = $id;
    }
 
    public function getId()
    {
        return $this->id;
    }
 





    public function __construct()
    {
      //  parent::__construct();
        // your own logic
    }

    /**
     * Set agencyName
     *
     * @param string $agencyName
     * @return Agency
     */
    public function setAgencyName($agencyName)
    {
        $this->agencyName = $agencyName;

        return $this;
    }

    /**
     * Get agencyName
     *
     * @return string 
     */
    public function getAgencyName()
    {
        return $this->agencyName;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Agency
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Agency
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Agency
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Agency
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Agency
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Agency
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set billingContact
     *
     * @param string $billingContact
     * @return Agency
     */
    public function setBillingContact($billingContact)
    {
        $this->billingContact = $billingContact;

        return $this;
    }

    /**
     * Get billingContact
     *
     * @return string 
     */
    public function getBillingContact()
    {
        return $this->billingContact;
    }

    /**
     * Set billingPhone
     *
     * @param string $billingPhone
     * @return Agency
     */
    public function setBillingPhone($billingPhone)
    {
        $this->billingPhone = $billingPhone;

        return $this;
    }
	 /**
     * Get billingPhone
     *
     * @return string 
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }



    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }



    /**
     * Set fax
     *
     * @param string $fax
     * @return string
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }




    /**
     * Set averageRating
     *
     * @param float $averageRating
     * @return float
     */
    public function setAverageRating($averageRating)
    {
        $this->averageRating = $averageRating;

        return $this;
    }

    /**
     * Get averageRating
     *
     * @return float 
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }



    /**
     * Set averageRatingCount
     *
     * @param integer $averageRatingCount
     * @return integer
     */
    public function setAverageRatingCount($averageRatingCount)
    {
        $this->averageRatingCount = $averageRatingCount;

        return $this;
    }

    /**
     * Get averageRatingCount
     *
     * @return integer 
     */
    public function getAverageRatingCount()
    {
        return $this->averageRatingCount;
    }



  	/**
     * Set averageOverallRating
     *
     * @param float $averageOverallRating
     * @return float
     */
    public function setAverageOverallRating($averageOverallRating)
    {
        $this->averageOverallRating = $averageOverallRating;

        return $this;
    }

    /**
     * Get averageOverallRating
     *
     * @return float 
     */
    public function getAverageOverallRating()
    {
        return $this->averageOverallRating;
    }
	

    /**
     * Get averageDocumentRating
     *
     * @return float 
     */
    public function getAverageDocumentRating()
    {
        return $this->averageDocumentRating;
    }
	/**
     * Set averageDocumentRating
     *
     * @param float $averageDocumentRating
     * @return float
     */
    public function setAverageDocumentRating($averageDocumentRating)
    {
        $this->averageDocumentRating = $averageDocumentRating;

        return $this;
    }


	    /**
     * Get averagePaymentRating
     *
     * @return float 
     */
    public function getAveragePaymentRating()
    {
        return $this->averagePaymentRating;
    }
	/**
     * Set averagePaymentRating
     *
     * @param float $averagePaymentRating
     * @return float
     */
    public function setAveragePaymentRating($averagePaymentRating)
    {
        $this->averagePaymentRating = $averagePaymentRating;

        return $this;
    }


	    /**
     * Get averagePayRateRating
     *
     * @return float 
     */
    public function getAveragePayRateRating()
    {
        return $this->averagePayRateRating;
    }
	/**
     * Set averagePayRateRating
     *
     * @param float $averagePayRateRating
     * @return float
     */
    public function setAveragePayRateRating($averagePayRateRating)
    {
        $this->averagePayRateRating = $averagePayRateRating;

        return $this;
    }


    /**
     * Get payrateRatingCount
     *
     * @return integer 
     */
    public function getPayrateRatingCount()
    {
        return $this->payrateRatingCount;
    }
	/**
     * Set payrateRatingCount
     *
     * @param integer $payrateRatingCount
     * @return integer
     */
    public function setPayrateRatingCount($payrateRatingCount)
    {
        $this->payrateRatingCount = $payrateRatingCount;

        return $this;
    }


/**
     * Get documentRatingCount
     *
     * @return integer 
     */
    public function getDocumentRatingCount()
    {
        return $this->documentRatingCount;
    }
	/**
     * Set documentRatingCount
     *
     * @param integer $documentRatingCount
     * @return integer
     */
    public function setDocumentRatingCount($documentRatingCount)
    {
        $this->documentRatingCount = $documentRatingCount;

        return $this;
    }

/**
     * Get paymentRatingCount
     *
     * @return integer 
     */
    public function getPaymentRatingCount()
    {
        return $this->paymentRatingCount;
    }
	/**
     * Set paymentRatingCount
     *
     * @param integer $paymentRatingCount
     * @return integer
     */
    public function setPaymentRatingCount($paymentRatingCount)
    {
        $this->paymentRatingCount = $paymentRatingCount;

        return $this;
    }


/**
     * Get overallRatingCount
     *
     * @return integer 
     */
    public function getOverallRatingCount()
    {
        return $this->overallRatingCount;
    }
	/**
     * Set overallRatingCount
     *
     * @param integer $overallRatingCount
     * @return integer
     */
    public function setOverallRatingCount($overallRatingCount)
    {
        $this->overallRatingCount = $overallRatingCount;

        return $this;
    }


    /**
     * Add ratings
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Rating $ratings
     * @return Agency
     */
    public function addRating(\CruzDigital\PayGradeBundle\Entity\Rating $ratings)
    {
        $this->ratings[] = $ratings;

        return $this;
    }

    /**
     * Remove ratings
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Rating $ratings
     */
    public function removeRating(\CruzDigital\PayGradeBundle\Entity\Rating $ratings)
    {
        $this->ratings->removeElement($ratings);
    }

    /**
     * Get ratings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRatings()
    {
        return $this->ratings;
    }
	    /**
     * Set website
     *
     * @param string $website
     * @return string
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }
	
	
	
}
