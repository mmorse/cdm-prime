<?php

namespace CruzDigital\PayGradeBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * AgencyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AgencyRepository extends EntityRepository
{
	public function findOneByAverageRating($agency_id)
    {
		//depreciated - moved to coreservice
	
        $query = $this->getEntityManager()
        ->createQuery(
            'SELECT AVG(ratingValue) AS ratavg FROM PayGradeBundle:Rating 
            WHERE agency_id = :id'
        )->setParameter('id', $agency_id);
		
		
		
		
		return $query->getSingleResult();
    }
}
