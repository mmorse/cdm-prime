<?php
// src/CruzDigital/PayGradeBundle/Entity/Vote.php

namespace CruzDigital\PayGradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CruzDigital\PayGradeBundle\Entity\VoteRepository")
 * @ORM\Table(name="vote")
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    protected $voteType;
	 /**
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="votes")
     * @var Comment|null
     */
     private $comment;
	
	 /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="votes")
     * @var Agency|null
     */
     private $agency;
	 /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="votes")
     * @var User|null
     */
     private $user;
	
	
	/**
   	* @ORM\Column(type="integer")
     *
     */
    protected $voteValue;


    public function __construct()
    {
        
        // your own logic
    }
	    //Setters, getters
     
    /** @return User|null */
    public function getUser() {
        return $this->user;
    }
     
    /** @param User $user */
    public function setUser(User $user) {
        if($user === null || $user instanceof User) {
            $this->user = $user;
        } else {
            throw new InvalidArgumentException('$user must be instance of Entity\User or null!');
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set voteType
     *
     * @param string $voteType
     * @return Vote
     */
    public function setVoteType($voteType)
    {
        $this->voteType = $voteType;

        return $this;
    }

    /**
     * Get voteType
     *
     * @return string 
     */
    public function getVoteType()
    {
        return $this->voteType;
    }

    /**
     * Set voteValue
     *
     * @param float $voteValue
     * @return Vote
     */
    public function setVoteValue($voteValue)
    {
        $this->voteValue = $voteValue;

        return $this;
    }

    /**
     * Get voteValue
     *
     * @return float 
     */
    public function getVoteValue()
    {
        return $this->voteValue;
    }

    /**
     * Set agency
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Agency $agency
     * @return Vote
     */
    public function setAgency(\CruzDigital\PayGradeBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \CruzDigital\PayGradeBundle\Entity\Agency 
     */
    public function getAgency()
    {
        return $this->agency;
    }
	
	
    /**
     * Set comment
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Comment $comment
     * @return Vote
     */
    public function setComment(\CruzDigital\PayGradeBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \CruzDigital\PayGradeBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
	
	
	
	
	
}