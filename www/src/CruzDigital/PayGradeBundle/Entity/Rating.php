<?php
// src/CruzDigital/PayGradeBundle/Entity/Rating.php

namespace CruzDigital\PayGradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CruzDigital\PayGradeBundle\Entity\RatingRepository")
 * @ORM\Table(name="rating")
 */
class Rating
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $ratingType;
	
	 /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="ratings")
     * @var User|null
     */
     private $agency;
	 /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ratings")
     * @var User|null
     */
     private $user;
	
	
	/**
   	* @ORM\Column(type="float", scale=2)
     *
     */
    protected $ratingValue;


    public function __construct()
    {
        
        // your own logic
    }
	    //Setters, getters
     
    /** @return User|null */
    public function getUser() {
        return $this->user;
    }
     
    /** @param User $user */
    public function setUser(User $user) {
        if($user === null || $user instanceof User) {
            $this->user = $user;
        } else {
            throw new InvalidArgumentException('$user must be instance of Entity\User or null!');
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ratingType
     *
     * @param string $ratingType
     * @return Rating
     */
    public function setRatingType($ratingType)
    {
        $this->ratingType = $ratingType;

        return $this;
    }

    /**
     * Get ratingType
     *
     * @return string 
     */
    public function getRatingType()
    {
        return $this->ratingType;
    }

    /**
     * Set ratingValue
     *
     * @param float $ratingValue
     * @return Rating
     */
    public function setRatingValue($ratingValue)
    {
        $this->ratingValue = $ratingValue;

        return $this;
    }

    /**
     * Get ratingValue
     *
     * @return float 
     */
    public function getRatingValue()
    {
        return $this->ratingValue;
    }

    /**
     * Set agency
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Agency $agency
     * @return Rating
     */
    public function setAgency(\CruzDigital\PayGradeBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \CruzDigital\PayGradeBundle\Entity\Agency 
     */
    public function getAgency()
    {
        return $this->agency;
    }
}