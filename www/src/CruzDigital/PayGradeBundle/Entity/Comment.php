<?php
// src/CruzDigital/PayGradeBundle/Entity/Comment.php

namespace CruzDigital\PayGradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="CruzDigital\PayGradeBundle\Entity\CommentRepository")
 * @ORM\Table(name="comment")
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $commentText;
	
	 /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="ratings")
     * @var User|null
     */
     private $agency;
	 /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ratings")
     * @var User|null
     */
     private $user;
	
	
	/**
   	* @ORM\Column(type="float", scale=2, nullable=true)
     *
     */
    protected $commentValue;
 	/**
     * @ORM\OneToOne(targetEntity="Comment")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;


	   /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="comment")
     */
    protected $votes;

    public function __construct()
    {
        
        $this->votes = new ArrayCollection();
    }
	    //Setters, getters
     
    /** @return User|null */
    public function getUser() {
        return $this->user;
    }
     
	   public function getId() {
        return $this->id;
    }
	 
    /** @param User $user */
    public function setUser(User $user) {
        if($user === null || $user instanceof User) {
            $this->user = $user;
        } else {
            throw new InvalidArgumentException('$user must be instance of Entity\User or null!');
        }
    }


    /**
     * Set agency
     *
     * @param \CruzDigital\PayGradeBundle\Entity\Agency $agency
     * @return agency
     */
    public function setAgency(\CruzDigital\PayGradeBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \CruzDigital\PayGradeBundle\Entity\Agency 
     */
    public function getAgency()
    {
        return $this->agency;
    }
	
public function setCommentText($commentText)
    {
        $this->commentText = $commentText;

        return $this;
    }
public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }
public function setCommentValue($commentValue)
    {
        $this->commentValue = $commentValue;

        return $this;
    }


public function getCommentText()
    {
       return $this->commentText;

       
    }
public function getParentId()
    {
      return $this->parentId;
    }
public function getCommentValue()
    {
       return $this->commentValue;

    }

}