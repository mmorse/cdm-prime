<?php
namespace CruzDigital\PayGradeBundle\Common;

class CoreServices
{
  protected $em;


    public function TotalVoteValue($comment_id)
    {
        # This will update the totals and save it in the commentValue column of each comment      
		
 		$query =  $this->em
        ->createQuery(
            'SELECT SUM( v.voteValue) AS votevalue FROM PayGradeBundle:Vote v WHERE v.comment=:commentid '
        )->setParameter('commentid', $comment_id);
		$myvotetotal=$query->getOneOrNullResult(); 
		if (!$myvotetotal ) { 
		$cleantotal=0; 
		} 
		else 
		{
			$cleantotal=$myvotetotal['votevalue']; 
			}
		
		return $cleantotal;
    }
	
	
    public function UserVoteValue($comment_id, $user_id)
    {
        # This will update the totals and save it in the commentValue column of each comment      
		
 		$query =  $this->em
        ->createQuery(
            'SELECT  v.voteValue AS votevalue FROM PayGradeBundle:Vote v WHERE v.comment=:commentid AND v.user=:userid '
        )->setParameters(array( 'commentid'=>$comment_id, 'userid'=>$user_id));
		$myvoteresult=$query->getOneOrNullResult();
		if (!$myvoteresult) { 
		$myvote=0; 
		} 
		else 
		{
			$myvote=$myvoteresult['votevalue']; 
			}
		
		
		return $myvote;
    }
	
	
	
	
	
	public function UpdateCommentValue($comment_id)
    {
        # This will update the totals and save it in the commentValue column of each comment      
		//totalvotes
		$commentid=$comment_id;
					 $comment = $this->em->getRepository('PayGradeBundle:Comment')
        			->find($commentid);
						
					
    				if (!$comment) {
        				throw $this->createNotFoundException(
            		'No comment found for id '.$id
        			);
    				} 
					else { 
					
						$votetotal = $this->TotalVoteValue($commentid);
					 	$comment->setCommentValue($votetotal);
   					 	$this->em->flush();
						return $votetotal;
					}
 					
		
    }
	
	
	public function GetAverageRating($agency_id) { 
		$em= $this->em; 
		
        $query = $em
        ->createQuery(
            'SELECT AVG(r.ratingValue) AS ratingavg, COUNT(DISTINCT r.user) AS ratingcount FROM PayGradeBundle:Rating r WHERE r.agency =:id')->setParameter('id', $agency_id);
		$myavgandcount=$query->getSingleResult();
		return $myavgandcount; 
		
	


	
	}
	
	
	public function UpdateAgencyRating($agency_id, $ratingType)
    {
        # This will update the averagerathing and rating count for the agency and rating type   
		//totalvotes
		$em= $this->em; 
		$agencyId=$agency_id;
		$ratingtype=$ratingType; 
				$query = $em->createQuery(
     				'SELECT AVG( r.ratingValue) AS ratingaverage, COUNT( r.ratingValue) AS ratingtotal FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid
					 AND r.ratingType =:ratingType'
					)->setParameters(array( 'agencyid'=>$agencyId, 'ratingType'=>$ratingType)); 
		$myagencyratings = $query->getResult();
		$myratingscount=count($myagencyratings); 
		
		if (!$myagencyratings) {
			$ratingaverage=0; 
			$ratingtotal=0; 
		 } 
			else { 
			$myarray=$myagencyratings[0]; 
			
			$ratingaverage=$myarray['ratingaverage'] ;
			$ratingtotal=$myarray['ratingtotal'];
				$myagency = $em->getRepository('PayGradeBundle:Agency')->find($agencyId);
			
				if($ratingtype=="overall" ) {
					
					$myagency ->setAverageOverallRating($ratingaverage);
					$myagency ->setOverallRatingCount($ratingtotal);
				
				}
				if($ratingtype=="payment" ) {
					
					$myagency ->setAveragePaymentRating($ratingaverage);
					$myagency ->setPaymentRatingCount($ratingtotal);
				
				}
				if($ratingtype=="documents" ) {
					$myagency ->setAverageDocumentRating($ratingaverage);
					$myagency ->setDocumentRatingCount($ratingtotal);
				
				}
				if($ratingtype=="payrate" ) {
					$myagency ->setAveragePayRateRating($ratingaverage);
					$myagency ->setPayRateRatingCount($ratingtotal);
				}
				$rat=$this->getAverageRating($agency_id);
				//compute new average rating and set it before updating entity
   				$myagency->setAverageRating( number_format((float)$rat['ratingavg'], 2, '.', '')); 
				$myagency->setAverageRatingCount($rat['ratingcount']);
				
				$em->flush(); 
				
				$agencyratingmeta=array('ratingaverage'=>$ratingaverage, 'ratingtotal'=>$ratingtotal);
				
				return $agencyratingmeta; 
				
			
			 } 
					
 					
		
    }
	
	public function findTopBySortType($sortType)
    {
        # This will return the top or bottom 10 agencies sorted by    
		//totalvotes
		$em= $this->em; 
		
		$sorttype=$sortType; 
				$query = $em->createQuery(
     				'SELECT a FROM PayGradeBundle:Agency a 
					 ORDER BY a.averageRating DESC'
					)  ->setFirstResult(0)
                       ->setMaxResults(10);
					
		$myagencies = $query->getResult();
		
		return $myagencies; 
	}
	public function findBottomBySortType($sortType)
    {
        # This will return the top or bottom 10 agencies sorted by    
		//totalvotes
		$em= $this->em; 
		
		$sorttype=$sortType; 
				$query = $em->createQuery(
     				'SELECT a FROM PayGradeBundle:Agency a WHERE (a.averageRatingCount >0)
					 ORDER BY a.averageRating ASC'
					)  ->setFirstResult(0)
                       ->setMaxResults(10);
					
		$myagencies = $query->getResult();
		
		return $myagencies; 
	}
	
	
	
	public function __construct(\Doctrine\ORM\EntityManager $em)
  {
    $this->em = $em;
  }
	
}