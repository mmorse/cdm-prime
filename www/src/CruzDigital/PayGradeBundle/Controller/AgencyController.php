<?php
// src/CruzDigital/PayGradeBundle/Controller/AgencyController.php
namespace CruzDigital\PayGradeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CruzDigital\PayGradeBundle\Form\Type\AgencyType;
use CruzDigital\PayGradeBundle\Entity\Agency; 
use CruzDigital\PayGradeBundle\Entity\Rating;
use CruzDigital\PayGradeBundle\Entity\User;
use CruzDigital\PayGradeBundle\Entity\Comment;
use CruzDigital\PayGradeBundle\Entity\Vote;
use CruzDigital\PayGradeBundle\Form\Type\BrowseType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;


class AgencyController extends Controller
{
   
public function createAction( Request $request )
  {
    $agencyform = $this->createForm( new AgencyType( ) );
	$request = $this->getRequest(); // replacement for getting the request
if ( $request->isMethod( 'POST' ) ) {
$agencyform->submit( $request ); // renamed from bind to submit, renamed $form to $postform
if ( $agencyform->isValid( ) ) { // renamed $form to $postform
/*
* $data['title']
* $data['body']
*/
$data = $agencyform->getData(); // renamed $form to $postform


   		$agency = new Agency();
   		$agency->setagencyName($data['agencyName']);
	  	$agency->setAddress1($data['address1']);
	   	$agency->setAddress2($data['address2']);
	    $agency->setCity($data['city']);
		$agency->setState($data['state']);
		$agency->setZip($data['zip']);
		$agency->setEmail($data['email']);
		$agency->setBillingContact($data['billingContact']);
		$agency->setBillingPhone($data['billingPhone']);
		$agency->setFax($data['fax']);
		$agency->setWebsite($data['website']);
		$agency->setAverageRating(0);
		$agency->setAverageRatingCount(0);
		$agency->setAverageOverallRating(0);
		$agency->setOverallRatingCount(0);
		$agency->setAveragePaymentRating(0);
		$agency->setPaymentRatingCount(0);
		$agency->setAverageDocumentRating(0);
		$agency->setDocumentRatingCount(0);
		$agency->setAveragePayRateRating(0);
		$agency->setPayRateRatingCount(0);
    $em = $this->getDoctrine()->getManager();
    $em->persist($agency);
    $em->flush();

$response['success'] = true;


}else{
$response['success'] = false;
$response['cause'] = 'whatever';
}
$myagencyid=$agency->getId();
$response = $this->forward('PayGradeBundle:Agency:view', array(
        'agencyId'  => $myagencyid
       
    ));

    // ... further modify the response or return it directly

    return $response;
}
 
	 return $this->render('PayGradeBundle:Agency:create.html.twig',
            array(
                'agencyform' => $agencyform->createView()
            ));
	
	
	
}



public function viewAction($agencyId)
{
	// Check Security Context  
	$usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
		$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
		
	}
	
	else{ 
	 return $this->redirect($this->generateUrl('fos_user_security_login'));
	}
	// get agency we are trying to view
	   	$repository = $this->getDoctrine()
        	->getRepository('PayGradeBundle:Agency');
 		$agency = $repository->findOneById($agencyId);
		
 	if (!$agency) {
        throw $this->createNotFoundException(
            'No product found for id '.$agencyId
        );
  	}
	$em = $this->getDoctrine()->getEntityManager();
 	$query = $em->createQuery(
     	'SELECT  r.ratingValue, r.id, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType'
     	)->setParameters(array( 'agencyid'=>$agencyId, 'myid'=>$myid, 'ratingType'=>'overall')); 
	$myagencyratings = $query->getResult();
	$myratingscount=count($myagencyratings); 
		if (!$myagencyratings) {
			$overallvalue=0 ; 
		 } 
			else { 
			$myarray=$myagencyratings[0]; 
			
			$overallvalue=$myarray['ratingValue'] ;
			 } 
	$query = $em->createQuery(
     	'SELECT  r.ratingValue, r.id, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType'
     	)->setParameters(array( 'agencyid'=>$agencyId, 'myid'=>$myid, 'ratingType'=>'payment')); 
	$myagencyratings = $query->getResult();
	$myratingscount=count($myagencyratings); 
		if (!$myagencyratings) {
			$paymentvalue=0 ; 
		 } 
			else { 
			
			$myarray=$myagencyratings[0]; 
			$paymentvalue=$myarray['ratingValue'] ;
			 } 
	$query = $em->createQuery(
     	'SELECT  r.ratingValue, r.id, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType'
     	)->setParameters(array( 'agencyid'=>$agencyId, 'myid'=>$myid, 'ratingType'=>'documents')); 
	$myagencyratings = $query->getResult();
	$myratingscount=count($myagencyratings); 
		if (!$myagencyratings) {
			$documentsvalue=0 ; 
		 } 
			else { 
			
			$myarray=$myagencyratings[0]; 
			$documentsvalue=$myarray['ratingValue'] ;
			 } 		 
	$query = $em->createQuery(
     	'SELECT  r.ratingValue, r.id, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType'
     	)->setParameters(array( 'agencyid'=>$agencyId, 'myid'=>$myid, 'ratingType'=>'payrate')); 
	$myagencyratings = $query->getResult();
	$myratingscount=count($myagencyratings); 
		if (!$myagencyratings) {
			$payratevalue=0 ; 
		 } 
			else { 
			
			$myarray=$myagencyratings[0]; 
			$payratevalue=$myarray['ratingValue'] ;
			 } 				 
			 

	 return $this->render('PayGradeBundle:Agency:view.html.twig',   array('notariety'=> $notariety, 'agency' => $agency, 'myid'=> $myid, 'mycount'=> $myratingscount, 'myratings'=>$myagencyratings,  'agencyid'=>$agencyId, 'overview'=>$overallvalue, 'paymentvalue'=>$paymentvalue, 'documentsvalue'=>$documentsvalue, 'payratevalue'=>$payratevalue));
	
	
}




public function overviewAction($agencyId)
{
	// Check Security Context  
	$usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
		$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
		
	}
	
	else{ 
	 return $this->redirect($this->generateUrl('fos_user_security_login'));
	}
	// get agency we are trying to view
	   	$repository = $this->getDoctrine()
        	->getRepository('PayGradeBundle:Agency');
 		$agency = $repository->findOneById($agencyId);
		
 	if (!$agency) {
        throw $this->createNotFoundException(
            'No product found for id '.$agencyId
        );
  	}
	

	 return $this->render('PayGradeBundle:Agency:overview.html.twig',   array('notariety'=> $notariety, 'agency' => $agency, 'myid'=> $myid));
	
	
}



public function topAction()
{
	// Check Security Context  
	$usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
		$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
		
	}
	
	else{ 
	 return $this->redirect($this->generateUrl('fos_user_security_login'));
	}
	// get agency we are trying to view
	   		
		$service = $this->get('cruzdigital.coreservices');
 					// Work with the service container..
						$agencylist=$service->findTopBySortType("DESC");
		
 	if (!$agencylist) {
        throw $this->createNotFoundException(
            'No List found for AgencyList '
        );
  	}
	

	 return $this->render('PayGradeBundle:Agency:browsetop.html.twig',   array('notariety'=> $notariety, 'agencylist' => $agencylist, 'myid'=> $myid));
	
	
}


public function bottomAction()
{
		// Check Security Context  
	$usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
		$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
		
	}
	
	else{ 
	 return $this->redirect($this->generateUrl('fos_user_security_login'));
	}
	// get agency we are trying to view
	   		
		$service = $this->get('cruzdigital.coreservices');
 					// Work with the service container..
						$agencylist=$service->findBottomBySortType("DESC");
		
 	if (!$agencylist) {
        throw $this->createNotFoundException(
            'No List found for AgencyList '
        );
  	}
	

	 return $this->render('PayGradeBundle:Agency:browsebottom.html.twig',   array('notariety'=> $notariety, 'agencylist' => $agencylist, 'myid'=> $myid));
	
	
	
}




  
public function browseAction( Request $request )
  {
	  
	  
	//  if (false === $this->get('security.context')->isGranted(
     //   'IS_AUTHENTICATED_FULLY'
     //  )) {
     //    	return $this->render('PayGradeBundle:Welcome:index.html.twig');
   // }
	  
	  
	  
	  
	   $browseform = $this->createForm( new BrowseType( ) );
 
if ( $request->isMethod( 'POST' ) ) {
	
//response codes: 

   $request = $this->get('request');
   $agencyName=$request->request->get('agencyName');
   
   if($agencyName!=""){//if the user has written his name
      $greeting='Hello '.$agencyName.'. How are you today?';
      $return=array("responseCode"=>200,  "greeting"=>$greeting);
   }
   else{
      $return=array("responseCode"=>400, "greeting"=>"You have to write your name!");
   }

   $return=json_encode($return);//jscon encode the array
   return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type	
	
	
	
	
	
//$browseform->submit( $request ); 

/*
* $data['title']
* $data['body']
*/
//$data = $browseform->getData(); // renamed $form to $postform
// $value = $request->get('term');

        // .... (Search values)
 //       $search = array(
   //         array('value' => 'genesis', 'label' => 'Genesis'),
     //       array('value' => 'exodus', 'label' => 'Exodus'),
	//		array('value' => 'leviticus', 'label' => 'leviticus')
     //   );

        //$response = new Response();
       // $response->setContent(json_encode($search));

        //return $response;

   	
$response['success'] = true;

return new JsonResponse( $response );
}
 	$usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") {  // if a user is logged in, get notariety
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	
	$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
	$notarietyTitle=$em->getRepository('PayGradeBundle:User')->findNotarietyTitlebyNotarietyPoints($notariety);
	}
	else {
	$notariety=0; 
	$notarietyTitle="Anonymous"; }
	 return $this->render('PayGradeBundle:Agency:browse.html.twig',
            array(
                'browseform' => $browseform->createView(), 'notariety'=>$notariety, 'notarietyTitle'=>$notarietyTitle
            ));
	
	
}


public function filterAction(){
   $request = $this->get('request');
   $name=$request->request->get('agencyName');
   
   if($name!=""){//if the user has written his name
   
   	$em = $this->getDoctrine()->getEntityManager();
 $query = $em->createQuery(
     'SELECT a.agencyName, a.id, a.city, a.state FROM PayGradeBundle:Agency a WHERE a.agencyName LIKE :searchTerm ORDER BY a.agencyName ASC'
     )->setParameter('searchTerm','%'. $name.'%');

$agencies = $query->getResult();


		
$averageratings=array(); 
$countratings=array(); 
$service = $this->get('cruzdigital.coreservices');
 					// Work with the service container...
	
foreach ( $agencies as $agency) {
	
	$rat=$service->getAverageRating($agency['id']);
	array_push($averageratings, number_format((float)$rat['ratingavg'], 2, '.', ''));  
	array_push($countratings, $rat['ratingcount']);  	
}



      $greeting='Hello '.$name.'. How are you today?';
      $return=array("responseCode"=>200,  "greeting"=>$greeting, "agencies"=>$agencies, "averageRating"=>$averageratings, "ratingCount"=>$countratings);
   }
   else{
      $return=array("responseCode"=>400, "greeting"=>"You have to write your name!");
   }

   $return=json_encode($return);//jscon encode the array
   return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
}



  
public function commentAction($agencyId)
{  
 //logged in? 	
$usr= $this->get('security.context')->getToken()->getUser();
if($usr!="anon.") 
{  // if a user is logged in, get notariety
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
	$notarietyTitle=$em->getRepository('PayGradeBundle:User')->findNotarietyTitlebyNotarietyPoints($notariety);
	$repository = $this->getDoctrine()
        		->getRepository('PayGradeBundle:Agency');
 			$agency = $repository->findOneById($agencyId);
	if (!$agency) 
			{
        		throw $this->createNotFoundException(
            	'No agency found for id '.$agencyId);
  			}	
			else 
			{ 
			
  			$request = $this->get('request');
 				if ( $request->isMethod( 'GET' ) ) {
					// check for existing comments - order them by rating
					$comments = $this->getDoctrine()
        			->getRepository('PayGradeBundle:Comment')
        			->findAllByAgencyId($agencyId);
					// if this is not a get or a post
					$votemeta=array(); 
					
					
					$service = $this->get('cruzdigital.coreservices');
 					// Work with the service container...
        			
        		
				  
        
					
					foreach ($comments as $comment) { 
					$votetotal = $service->TotalVoteValue($comment['id']);
					$uservote = $service->UserVoteValue($comment['id'], $myid); // will return -1, 0, or 1
					array_push($votemeta, array("votetotal"=> $votetotal, "uservote"=>$uservote) );  	
					
					
					
					} 
				
				
					return $this->render('PayGradeBundle:Agency:comment.html.twig',
            		array('agency'=>$agency, 'comments'=>$comments, 'agencyid'=>$agencyId,
          			'notariety'=>$notariety, 'notarietyTitle'=>$notarietyTitle, 'votemeta'=>$votemeta
            		));		
  					}
			
			
			}
	if ( $request->isMethod( 'POST' ) ) {
	//is this an add comment or up/downvote ??	
	// add comment at appropriate spot on list
	$request = $this->get('request');
   	$commentText=$request->request->get('commentText');
	$ParentId=$request->request->get('parentId');
		if ($commentText !="" ) { 
					$newcomment = new Comment();
					$newcomment->setCommentText($commentText);
					$newcomment->setCommentValue(1);
					$newcomment->setUser($usr);
					$newcomment->setAgency($agency);
					//$newcomment->setParent($parentId);
					$em = $this->getDoctrine()->getManager();
					$em->persist($newcomment);
					$em->flush();
					
					
					$newvote = new Vote();
					$newvote->setVoteType("version1");
					$newvote->setVoteValue(1);
					$newvote->setUser($usr);
					$newvote->setAgency($agency);
					$newvote->setComment($newcomment);
					$em = $this->getDoctrine()->getManager();
					$em->persist($newvote);
					$em->flush();
					
					
		}	
	// add initial vote on comment for the user....(they get the upvote and point for it.) 


	
	// get existing comment list ordered by value
	$comments = $this->getDoctrine()
        			->getRepository('PayGradeBundle:Comment')
        			->findAllByAgencyId($agencyId);

   		$greeting='Comment Updated';
		
		$votemeta=array(); 	
					$service = $this->get('cruzdigital.coreservices');
 					// Work with the service container...
 
					foreach ($comments as $comment) { 
					$votetotal = $service->TotalVoteValue($comment['id']);
					if (!$votetotal) {$votetotal=0; }; 
					$uservote = $service->UserVoteValue($comment['id'], $myid); // will return -1, 0, or 1
					array_push($votemeta, array("votetotal"=> $votetotal, "uservote"=>$uservote) );  	
		
					} 
   		
	
	//response codes: 
		
	 $return=array("responseCode"=>200,  "greeting"=>$greeting, "comments"=>$comments, 'votemeta'=>$votemeta);
	$return=json_encode($return);//jscon encode the array
	//$returna= $serializer->serialize($return, 'json');
   return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
		//return new JsonResponse( $response );
	}
	else { 
	//agecncy selected
	//not post
 
	 return $this->render('PayGradeBundle:Agency:comment.html.twig',
            array('agency'=>$agency, 'comments'=>$comments, 'agencyid'=>$agencyId,
          'notariety'=>$notariety, 'notarietyTitle'=>$notarietyTitle
            ));
	
	} 
	
	}
	else {
	$notariety=0; 
	$notarietyTitle="Anonymous"; 
	}
	
	
	
}



public function rateAction(){
	
   
   $request = $this->get('request');
   $ratingid=$request->request->get('ratingid');
   $ratingtype=$request->request->get('ratingtype');
   $ratingagencyid=$request->request->get('agencyid');
   $ratinguserid=$request->request->get('userid');
   $ratingvalue=$request->request->get('ratingvalue');
  
   	//get agency by id
		$em1 = $this->getDoctrine()->getManager();
    	$ratingagency = $em1->getRepository('PayGradeBundle:Agency')->find($ratingagencyid);

    		if (!$ratingagency) {
        		throw $this->createNotFoundException(
            	'No product found for id '.$ratinagencyid
        		);
			}
   		$usr= $this->get('security.context')->getToken()->getUser();
		$myid=$usr->getId();
		if ($myid!=$ratinguserid) {
        		throw $this->createNotFoundException(
            	'Usernames do not match' 
        		);
			}
			else {
				$ratinguser=$myid=$usr->getId();
				 } 
 
   if($ratingid!=""){
	   if ($ratingid!="0") { 
	 
	   		//verify the right rating is selected 
	   		$em = $this->getDoctrine()->getEntityManager();
 			$query = $em->createQuery(
     		'SELECT r.id, r.ratingValue, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType AND r.id=:ratingId'
     		)->setParameters(array( 'agencyid'=>$ratingagencyid, 'myid'=>$myid,  'ratingType'=>$ratingtype, 'ratingId'=>$ratingid)); 
			$myagencyratings = $query->getResult();
			$myratingscount=count($myagencyratings); 
				if ($myagencyratings==0) {
			
					$newrating = new Rating();
					$newrating->setRatingType($ratingtype);
					$newrating->setRatingValue($ratingvalue);
					$newrating->setUser($usr);
					$newrating->setAgency($ratingagency);
					
					$em = $this->getDoctrine()->getManager();
					$em->persist($newrating);
					$em->flush();

		 		} 
				else 
				{ 
			
				
				$em2 = $this->getDoctrine()->getManager();
   				 $myrating = $em2->getRepository('PayGradeBundle:Rating')->find($myagencyratings[0]);
				 $myrating ->setRatingValue($ratingvalue);
				 $em2->flush();
				

				
				} 
	   } 
	   
	   else { 
	
   		//if this is supposed to be a new rating, check if it really is new, if so, create, if not, update
	   		$em = $this->getDoctrine()->getEntityManager();
 			$query = $em->createQuery(
     		'SELECT r.id, r.ratingValue, r.ratingType FROM PayGradeBundle:Rating r WHERE r.agency  =:agencyid AND r.user = :myid AND r.ratingType =:ratingType' 
     		)->setParameters(array( 'agencyid'=>$ratingagencyid, 'myid'=>$myid, 'ratingType'=>$ratingtype)); 
			$myagencyratings = $query->getResult();
			$myratingscount=count($myagencyratings); 
				if ($myratingscount==0) {
				//said it was new, and it really was:  
			
					$newrating = new Rating();
					$newrating->setRatingType($ratingtype);
					$newrating->setRatingValue($ratingvalue);
					$newrating->setUser($usr);
					$newrating->setAgency($ratingagency);
					
					$em = $this->getDoctrine()->getManager();
					$em->persist($newrating);
					$em->flush();

				
		 		} 
				else 
				{ 
			
				
				$em2 = $this->getDoctrine()->getManager();
   				 $myrating = $em2->getRepository('PayGradeBundle:Rating')->find($myagencyratings[0]);
				 $myrating ->setRatingValue($ratingvalue);
				 $em2->flush();
		

   
				} 
   		
   
   
   
   
	   }
	   $usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
	$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
	
	$service = $this->get('cruzdigital.coreservices');
	$agencyratingmeta = $service->UpdateAgencyRating($ratingagencyid, $ratingtype);
	
	
	}
	    $return=array("responseCode"=>200, "greeting"=>"Success", 'notariety'=>$notariety, '$agencyratingmeta'=>$agencyratingmeta);
   }
   else{
      $return=array("responseCode"=>400, "greeting"=>"You have to write your name!");
   }

   $return=json_encode($return);//jscon encode the array
   return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
}


public function voteAction(){
	
   
   $request = $this->get('request');
   $commentid=$request->request->get('commentid');
   $votetype="version1";
   $voteagencyid=$request->request->get('agencyid');
   $voteuserid=$request->request->get('userid');
   $votevalue=$request->request->get('votevalue');
  
   	//get agency by id
		$em1 = $this->getDoctrine()->getManager();
    	$voteagency = $em1->getRepository('PayGradeBundle:Agency')->find($voteagencyid);
		$votecomment = $em1->getRepository('PayGradeBundle:Comment')->find($commentid);
    		if (!$voteagency) {
        		throw $this->createNotFoundException(
            	'No Agency found for id '.$voteagencyid
        		);
			}
   		$usr= $this->get('security.context')->getToken()->getUser();
		$myid=$usr->getId();
		if ($myid!=$voteuserid) {
        		throw $this->createNotFoundException(
            	'Usernames do not match' 
        		);
			}
			else {
				$voteuser=$myid=$usr->getId();
				 } 
 
   if($commentid!=""){
	   if ($commentid!="0") { 
	 
	   		//verify the right Vote is selected 
	   		$em = $this->getDoctrine()->getEntityManager();
 			$query = $em->createQuery(
     		'SELECT v.id, v.voteValue FROM PayGradeBundle:Vote v WHERE v.agency  =:agencyid AND v.user = :myid AND v.comment=:commentid AND v.voteType =:voteType'
     		)->setParameters(array( 'agencyid'=>$voteagencyid, 'myid'=>$myid,  'voteType'=>$votetype, 'commentid'=>$commentid)); 
			$myagencyvotes = $query->getResult();
			$myvotescount=count($myagencyvotes); 
			//if a vote isn't selected, create it....
				if ($myagencyvotes==0 | !$myagencyvotes) {
					if (!$votevalue) {$votevalue=0;}; 
			
					$newvote = new Vote();
					$newvote->setVoteType($votetype);
					$newvote->setVoteValue($votevalue);
					$newvote->setUser($usr);
					$newvote->setAgency($voteagency);
					$newvote->setComment($votecomment);
					$em = $this->getDoctrine()->getManager();
					$em->persist($newvote);
					$em->flush();

				

		 		} 
				else 
				{ 
				//if the vote is found - set its value
				if (!$votevalue) {$votevalue=0;}; 
				
				$em = $this->getDoctrine()->getManager();
   				 $myvote = $em->getRepository('PayGradeBundle:Vote')->find($myagencyvotes[0]);
				 $myvote ->setVoteValue($votevalue);
				 $em->flush();
				//total votes? 

				
				} 
	   } 
	   
	   else { 
	
   		//if this is supposed to be a new rating, check if it really is new, if so, create, if not, update
	   		$em = $this->getDoctrine()->getEntityManager();
 			$query = $em->createQuery(
     		'SELECT v.id, V.voteValue, v.voteType FROM PayGradeBundle:Vote v WHERE v.agency  =:agencyid AND v.user = :myid AND v.comment=:commentid AND v.voteType =:voteType' 
     		)->setParameters(array( 'agencyid'=>$voteagencyid, 'myid'=>$myid, 'voteType'=>$votetype)); 
			$myagencyvotes = $query->getResult();
			$myvotescount=count($myagencyvotes); 
				if ($myvotescount==0) {
				//said it was new, and it really was:  
		
					if (!$votevalue) {$votevalue=0;}; 
					$newvote = new Vote();
					$newvote->setVoteType($votetype);
					$newvote->setVoteValue($votevalue);
					$newvote->setUser($usr);
					$newvote->setAgency($voteagency);
					$newvote->setComment($commentid);
					$em = $this->getDoctrine()->getManager();
					$em->persist($newvote);
					$em->flush();
				
				
				 	//total votes?
				
				
					
				
					
		 		} 
				else 
				{ 
				if (!$votevalue) {$votevalue=0;}; 
				
				$em = $this->getDoctrine()->getManager();
   				 $myvote = $em->getRepository('PayGradeBundle:Vote')->find($myagencyvotes[0]);
				 $myvote ->setVoteValue($votevalue);
				 $em->flush();
				
				//total votes
				  
				

   
				} 
   		
   
   
   
   
	   }
	   $usr= $this->get('security.context')->getToken()->getUser();
	if($usr!="anon.") { 
	$myid=$usr->getId();
	$em = $this->getDoctrine()->getEntityManager();
	//$query = $em->createQuery(
    // 	'SELECT r.id FROM PayGradeBundle:Rating r WHERE r.user = :myid'
    // 	) ->setParameter('myid', $myid);
	//$myagencyratings = $query->getResult();
	$notariety=$em->getRepository('PayGradeBundle:User')->findNotarietybyUserId($myid);
		
	}
				// Total Vote Values
	
						$service = $this->get('cruzdigital.coreservices');
						$votetotal = $service->UpdateCommentValue($commentid);
						$uservote = $service->UserVoteValue($commentid, $myid); // will return -1, 0, or 1
	    $return=array("responseCode"=>200, "greeting"=>"Success", 'notariety'=>$notariety, 
		'votetotal'=>$votetotal, 'commentid'=>$commentid, 'uservote'=>$uservote);
   }
   else{
      $return=array("responseCode"=>400, "greeting"=>"You have to write your name!");
   }

   $return=json_encode($return);//jscon encode the array
   return new Response($return,200,array('Content-Type'=>'application/json'));//make sure it has the correct content type
}






}