<?php
// src/CruzDigital/PayGradeBundle/Controller/AccountController.php
namespace CruzDigital\PayGradeBundle\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;


class AccountController extends Controller
{
   

public function updateschemaAction() { 
$kernel = $this->get('kernel');

            $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
            $application->setAutoExit(false);
            //Create de Schema 
            $options = array('command' => 'doctrine:schema:update',"--force" => true);
            $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
            //Loading Fixtures
           // $options = array('command' => 'doctrine:fixtures:load',"--append" => true);
            $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
			
			return $this->render('PayGradeBundle:Welcome:index.html.twig');
			 
			 

}
public function goodbyeAction() { 
	$response = new Response();
	$response->headers->clearCookie('REMEMBERME');
	 $response->send();
$session = $this->getRequest()->getSession();
$ses_vars = $session->all();
foreach ($ses_vars as $key => $value) {
    $session->remove($key);
}
$this->get('security.context')->setToken(null);
$this->get('request')->getSession()->invalidate();
 return $this->render('PayGradeBundle:Welcome:goodbye.html.twig');
 
 
}

}

