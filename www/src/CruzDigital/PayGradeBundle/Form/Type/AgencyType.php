<?php
namespace CruzDigital\PayGradeBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
 
class AgencyType extends AbstractType
{
  public function buildForm( FormBuilderInterface $builder,
                                            array $options )
  {
	$builder->add( 'agencyName',  'text' );
    $builder->add( 'address1',  'text' );
	$builder->add( 'address2',  'text' );
	$builder->add( 'city',  'text' );
	$builder->add( 'state',  'text' );
	$builder->add( 'zip',  'text' );
	$builder->add( 'email',  'text' );
	$builder->add( 'billingContact',  'text' );
	$builder->add( 'billingPhone',  'text' );
	$builder->add( 'fax',  'text' );
	$builder->add( 'website',  'text' );
	
	$builder->add('save', 'submit');

  }
 
  function getName() {
    return 'AgencyType';
  }
}
