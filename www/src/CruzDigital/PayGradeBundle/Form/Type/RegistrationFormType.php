<?php
namespace CruzDigital\PayGradeBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('firstName');
		 $builder->add('lastName');
		 $builder->add('notoriety', 'hidden', (array(
    'data' => '0')));  
    }

    public function getName()
    {
        return 'cruzdigital_user_registration';
    }
}