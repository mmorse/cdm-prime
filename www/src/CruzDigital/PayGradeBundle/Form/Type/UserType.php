<?php
// src/CruzDigital/PayGradeBundle/Form/Type/UserType.php
namespace CruzDigital\PayGradeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email');
        $builder->add('plainPassword', 'repeated', array(
           'first_name'  => 'firstName',
           'second_name' => 'lastName'
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CruzDigital\PayGradeBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}