<?php
namespace CruzDigital\PayGradeBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
 
class BrowseType extends AbstractType
{
  public function buildForm( FormBuilderInterface $builder,
                                            array $options )
  {
	 $builder
        ->add('ajax_simple', 'genemu_jqueryautocompleter_choice', array(
            'route_name' => 'browse_agency'
        ));
	$builder->add('save', 'submit');

  }
 
  function getName() {
    return 'BrowseType';
  }
}
