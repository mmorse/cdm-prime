<?php
// src/CruzDigital/PayGradeBundle/Form/Type/RegistrationType.php
namespace CruzDigital\PagGradeBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('firstName');
		 $builder->add('lastName');
    }

    public function getName()
    {
        return 'cruzdigita_user_registration';
    }
}