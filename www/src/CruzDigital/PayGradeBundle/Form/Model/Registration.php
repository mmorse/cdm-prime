<?php
// src/CruzDigital/PayGradeBundle/Form/Model/Registration.php
namespace CruzDigital\PayGradeBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use CruzDigital\PayGradeBundle\Entity\User;

class Registration
{
    /**
     * @Assert\Type(type="CruzDigital\PayGradeBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @Assert\NotBlank()
     * @Assert\True()
     */
    protected $termsAccepted;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = (Boolean) $termsAccepted;
    }
}